import {LoggingService} from './logging.service';
import {Injectable,EventEmitter} from '@angular/core';

// this is necessary to access a service within a service
@Injectable()
export class AccountsService {
  accounts = [
    {
      name: 'Master Account',
      status: 'active'
    },
    {
      name: 'Testaccount',
      status: 'inactive'
    },
    {
      name: 'Hidden Account',
      status: 'unknown'
    }
  ];
  statusUpdated = new EventEmitter<string>();

  // realize you cannot use this by itself
  // needs metadata attached, like @Component, @Directive do
  constructor(private loggingService:LoggingService) {}

  addAccount(name:string,status:string) {
  	this.accounts.push({name:name,status:status});
  	this.loggingService.logStatusChange(status);
  }
  updateStatus(id:number,status:string) {
  	this.accounts[id].status = status;
  	this.loggingService.logStatusChange(status);
  }
}