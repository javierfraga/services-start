import { Component } from '@angular/core';
// Don't need to emit an Output event anymore
// import { Component, EventEmitter, Output } from '@angular/core';
import {LoggingService} from '../logging.service';
import {AccountsService} from '../accounts.service';

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css'],
  // this was overwriting the instance of service in app.component
  // providers: [LoggingService,AccountsService],
  // providers: [LoggingService],
})
export class NewAccountComponent {
  // Going to use service instead of emitting an event
  // @Output() accountAdded = new EventEmitter<{name: string, status: string}>();

  constructor(
    private loggingService:LoggingService,
    private accountsService:AccountsService,
  ) {
    this.accountsService.statusUpdated.subscribe(
      (status:string) => alert('New Status: ' + status)
    );
  }

  onCreateAccount(accountName: string, accountStatus: string) {
    // Going to use service instead of emitting an event
    // this.accountAdded.emit({
    //   name: accountName,
    //   status: accountStatus
    // });
    this.accountsService.addAccount(accountName,accountStatus);
    /*
     * will add this to service
     */
    // console.log('A server status changed, new status: ' + accountStatus);
    /*
     * you should not do this manually even though it does work
     */
    // const service = new LoggingService();
    // service.logStatusChange(accountStatus);
    /*
     * you should use like this instead
     * but then i take out again since will use with service within service
     */
     // this.loggingService.logStatusChange(accountStatus);
  }
}
