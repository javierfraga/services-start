import { Component, Input } from '@angular/core';
// import { Component, EventEmitter, Input, Output } from '@angular/core';
import {LoggingService} from '../logging.service';
import {AccountsService} from '../accounts.service';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  // this was overwriting the instance of service in app.component
  // providers: [LoggingService,AccountsService],
  // providers: [LoggingService],
})
export class AccountComponent {
  @Input() account: {name: string, status: string};
  @Input() id: number;
  // @Output() statusChanged = new EventEmitter<{id: number, newStatus: string}>();

  constructor(
    private loggingService:LoggingService,
    private accountsService:AccountsService,
  ) {}

  onSetTo(status: string) {
    // move this to the service
    // this.statusChanged.emit({id: this.id, newStatus: status});
    this.accountsService.updateStatus(this.id,status);    
    // move this to the service
    // console.log('A server status changed, new status: ' + status);
    /*
     * you should not do this manually even though it does work
     */
    // const service = new LoggingService();
    // service.logStatusChange(accountStatus);
    /*
     * you should use like this instead
     * but then i take out again since will use with service within service
     */
     // this.loggingService.logStatusChange(status);
     /*
      * watch how we can emit an @Output event in another service
      */
      this.accountsService.statusUpdated.emit(status);
  }
}
